package com.lagrange.usecase.model.user;

public interface UserDto {
    String getPseudo();

    String getPassword();
}
